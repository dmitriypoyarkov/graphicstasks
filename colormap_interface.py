import sys, os
import cv2 as cv
from PyQt5.QtWidgets import QApplication, QComboBox, QGraphicsView, QLayout, QRubberBand, QSizePolicy, QWidget, QLabel, QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtCore import QPoint, QRect, QSize, QSizeF, Qt, QEvent, pyqtSignal
from PyQt5.QtGui import QPixmap, QWindow

import graphics_task1 as swapper
import graphics_task2 as colormap


class SelectableImageLabel(QLabel):
    rectChanged = pyqtSignal(QRect)
    def __init__(self, controllable_label : QLabel, ):
        super().__init__()

        self.setAlignment(Qt.AlignCenter)
        self.setText('\n\n Image \n\n')
        self.setStyleSheet('''
            QLabel{
                border: 4px dashed #aaa
            }
        ''')
        self.my_pixmap = QPixmap()
        super().setMaximumSize(1400, 1400)
        # super().setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        # super().setScaledContents(True)

        self.rubberBand = QRubberBand(QRubberBand.Rectangle, self)
        self.setMouseTracking(True)
        self.origin = QPoint()
        self.changeRubberBand = False

        self.controllable_label = controllable_label

    def setPixmap(self, image : QPixmap):
        self.my_pixmap = image
        super().setPixmap(image.scaled(self.width(), self.height(), aspectRatioMode=Qt.KeepAspectRatio))
        super().resize(pixmap.size())
    
    def resizeEvent(self, event):
        if (event.type() == QEvent.Resize):
            if self.my_pixmap.isNull:
                return
            super().setPixmap(self.my_pixmap.scaled(self.width(), self.height(), aspectRatioMode=Qt.KeepAspectRatio))

    def mousePressEvent(self, event):
        self.origin = event.pos()
        self.rubberBand.setGeometry(QRect(self.origin, QSize()))
        self.rectChanged.emit(self.rubberBand.geometry())
        self.rubberBand.show()
        self.changeRubberBand = True

    def mouseMoveEvent(self, event):
        if self.changeRubberBand:
            x = min(self.size().width(), max(0, event.pos().x()))
            y = min(self.size().height(), max(0, event.pos().y()))
            bottom_right = QPoint(x, y)
            self.rubberBand.setGeometry(QRect(self.origin, bottom_right).normalized())
            self.rectChanged.emit(self.rubberBand.geometry())

    def mouseReleaseEvent(self, event):
        self.changeRubberBand = False
        reverse_scale_h = self.my_pixmap.size().height()/self.size().height()
        reverse_scale_w = self.my_pixmap.size().width()/self.size().width()
        rubber_rect = QRect(self.rubberBand.geometry())
        new_size = QSize(int(rubber_rect.size().width() * reverse_scale_w),
                         int(rubber_rect.size().height() * reverse_scale_h))
        new_origin = (int(self.origin.x()*reverse_scale_w),
                      int(self.origin.y()*reverse_scale_h))
        cropped_pixmap = self.my_pixmap.copy(QRect(new_origin[0], new_origin[1], new_size.width(), new_size.height()))
        self.controllable_label.setPixmap(cropped_pixmap)




class ViewImageLabel(QLabel):
    def __init__(self):
        super().__init__()

        self.setAlignment(Qt.AlignCenter)
        self.setText('\n\n Image \n\n')
        self.setStyleSheet('''
            QLabel{
                border: 4px dashed #aaa
            }
        ''')
        self.my_pixmap = QPixmap()
        super().setMaximumSize(1400, 1400)
        # super().setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        # super().setScaledContents(True)

    def setPixmap(self, image : QPixmap):
        self.my_pixmap = image
        super().setPixmap(image.scaled(self.width(), self.height(), aspectRatioMode=Qt.KeepAspectRatio))
        super().resize(pixmap.size())
    
    def resizeEvent(self, event):
        if (event.type() == QEvent.Resize):
            if self.my_pixmap.isNull:
                return
            super().setPixmap(self.my_pixmap.scaled(self.width(), self.height(), aspectRatioMode=Qt.KeepAspectRatio))


class FlexibleViewImageLabel(QLabel):
    def __init__(self):
        super().__init__()

        self.setAlignment(Qt.AlignCenter)
        self.setText('\n\n Image \n\n')
        self.setStyleSheet('''
            QLabel{
                border: 4px dashed #aaa
            }
        ''')
        self.my_pixmap = QPixmap()
        super().setMaximumSize(1400, 1400)
        # super().setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        # super().setScaledContents(True)

    def setPixmap(self, image : QPixmap):
        self.my_pixmap = image
        super().setPixmap(image.scaled(self.width(), self.height(), aspectRatioMode=Qt.KeepAspectRatio))
        # super().resize(pixmap.size())
    
    def resizeEvent(self, event):
        # if (event.type() == QEvent.Resize):
        #     if self.my_pixmap.isNull:
        #         return
        #     super().setPixmap(self.my_pixmap.scaled(self.width(), self.height(), aspectRatioMode=Qt.KeepAspectRatio))
        pass


class DragDropField(QWidget):
    def __init__(self):
        super().__init__()
        self.resize(400, 400)
        self.setAcceptDrops(True)

        mainLayout = QHBoxLayout()
        self.colors_to_swap : swapper.ColorsToSwap = swapper.ColorsToSwap.RED_BLUE
        self.img_path : str = ''

        self.dropImageField = DropImageLabel()
        self.showImageField = ViewImageLabel()
        mainLayout.addWidget(self.dropImageField)
        mainLayout.addWidget(self.showImageField)

        self.setLayout(mainLayout)

    def dragEnterEvent(self, event):
        if event.mimeData().hasImage:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasImage:
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasImage:
            event.setDropAction(Qt.CopyAction)
            file_path = event.mimeData().urls()[0].toLocalFile()
            self.img_path = file_path
            self.set_drop_field()
            self.set_show_field()
            event.accept()
        else:
            event.ignore()

    def set_drop_field(self):
        self.dropImageField.setPixmap(QPixmap(self.img_path))

    def set_show_field(self):
        if self.img_path == '':
            return
        new_img = swapper.swap_colors(self.img_path, self.colors_to_swap)
        file_name, *_, file_ext = os.path.splitext(self.img_path)
        swapped_file_path = file_name + '_swapped.' + file_ext
        cv.imwrite(swapped_file_path, new_img)
        self.showImageField.setPixmap(QPixmap(swapped_file_path))

    def set_colors_to_swap(self, colors : swapper.ColorsToSwap):
        self.colors_to_swap = colors



class DropDown(QWidget):
    def __init__(self, controllable_widget : DragDropField, parent = None):
        super().__init__(parent)
        
        layout = QHBoxLayout()
        self.cb = QComboBox()
        self.cb.addItem('Red-Blue')
        self.cb.addItem('Red-Green')
        self.cb.addItem('Blue-Green')
        self.cb.currentIndexChanged.connect(self.selectionchange)
        
        layout.addWidget(self.cb)
        self.setLayout(layout)
        self.setWindowTitle("combo box demo")

        self.controllable_widget = controllable_widget

    def selectionchange(self,i):
        print("Current index", i, "selection changed to", self.cb.currentText())
        new_colors = swapper.ColorsToSwap(i)
        self.controllable_widget.set_colors_to_swap(new_colors)
        self.controllable_widget.set_show_field()
      

if __name__ == '__main__':
    app = QApplication(sys.argv)
    
    window = QWidget()
    window.setWindowTitle('Magnet Field Colormap')

    layout = QGridLayout()
    
    title1 = QLabel('Country coastmap:')
    title1.setMaximumHeight(40)
    layout.addWidget(title1, 0, 0)

    title2 = QLabel('Selected area:')
    title1.setMaximumHeight(40)
    layout.addWidget(title2, 0, 1)

    selected_area_image = FlexibleViewImageLabel()
    selected_area_image.resize(400, 400)
    layout.addWidget(selected_area_image, 1, 1)

    country_image = SelectableImageLabel(selected_area_image)
    country_image.resize(1000, 1000)
    pixmap = QPixmap(colormap.COASTLINE_IMAGE_PATH)
    #selectable = SelectableView(country_image)
    country_image.setPixmap(pixmap)
    layout.addWidget(country_image, 1, 0)

    window.setLayout(layout)

    #label.show()
    window.show()

    country_image.show()
    sys.exit(app.exec_())