# Цветовая карта
# Есть привязка каждого пикселя к координате
# Можем выбрать инетересующий регион
# Зная координаты, можем из большого набора данных 
# выбрать ту часть данных, которая содержит эти 
# координаты.
import numpy as np
import cv2 as cv
import requests as req
from typing import Dict, List, Tuple
from colormap_folium import draw_on_map


COUNTRY_BORDERS_LATITUDE = (41, 82) # degrees, rounded
COUNTRY_BORDERS_LONGITUDE = (19, 169)
#COASTLINE_IMAGE_PATH = '/home/python/coastline.png'
COASTLINE_IMAGE_PATH = '/home/fc/python/coastline.png'
TEMP_PATH = './temp/'


class CountryCoastline(object):
    def __init__(self, latitude_interval : Tuple[int],
                 longitude_interval : Tuple[int],
                 coastline_image : np.ndarray):
        self.latitude_interval = latitude_interval
        self.longitude_interval = longitude_interval
        self.coastline_image = coastline_image

    def horizontal_img_pixel(self, longitude : int) -> int:
        country_width = self.longitude_interval[1] - self.longitude_interval[0]
        percent = (longitude - self.longitude_interval[0]) / country_width
        img_width = self.coastline_image.shape[0]
        return int(percent * img_width)

    def vertical_img_pixel(self, latitude : int) -> int:
        country_height = self.latitude_interval[1] - self.latitude_interval[0]
        percent = (latitude - self.latitude_interval[0]) / country_height
        img_height = self.coastline_image.shape[1]
        return int(percent * img_height)

    def horizontal_img_interval(self, longitude_interval : Tuple[int]) -> Tuple[int]:
        if (longitude_interval[0] < self.longitude_interval[0] or
            longitude_interval[1] > self.longitude_interval[1]):
            raise Exception('invalid interval')
        country_width = self.longitude_interval[1] - self.longitude_interval[0]
        start_percent = (longitude_interval[0] - self.longitude_interval[0])/ country_width
        end_percent = (longitude_interval[1] - self.longitude_interval[0])/ country_width

        img_width = self.coastline_image.shape[1]
        return (int(start_percent * img_width), int(end_percent * img_width))

    def vertical_img_interval(self, latitude_interval : Tuple[int]) -> Tuple[int]:
        if (latitude_interval[0] < self.latitude_interval[0] or
            latitude_interval[1] > self.latitude_interval[1]):
            raise Exception('invalid interval')
        country_height = self.latitude_interval[1] - self.latitude_interval[0]
        start_percent = (latitude_interval[0] - self.latitude_interval[0])/ country_height
        end_percent = (latitude_interval[1] - self.latitude_interval[0])/ country_height

        img_height = self.coastline_image.shape[0]
        return (int(start_percent * img_height), int(end_percent * img_height))

    def latitude_from_img(self, img_interval : Tuple[int]) -> Tuple[int]:
        img_height = self.coastline_image.shape[0]
        start_percent = int(img_interval[0] / img_height)
        end_percent = int(img_interval[1] / img_height)

        country_height = self.latitude_interval[1] - self.latitude_interval[0]
        return (self.latitude_interval[0] + start_percent * country_height,
                self.latitude_interval[0] + end_percent * country_height)

    def longitude_from_img(self, img_interval : Tuple[int]) -> Tuple[int]:
        img_width = self.coastline_image.shape[1]
        start_percent = int(img_interval[0] / img_width)
        end_percent = int(img_interval[1] / img_width)

        country_width = self.longitude_interval[1] - self.longitude_interval[0]
        return (self.longitude_interval[0] + start_percent * country_width,
                self.longitude_interval[0] + end_percent * country_width)


def parse_and_get_indices(response : str, lat_interval : Tuple[int], lon_interval : Tuple[int]) -> Tuple[Tuple[int]]:
    response = response.split('---------------------------------------------')[1]
    response = response.split('\n')
    latitudes = response[2]
    longitudes = response[5]
    #print(response[5])
    lat_start : int = None
    lat_end : int = None
    for i, latitude in enumerate(latitudes.split(', ')):
        latitude = float(latitude)
        if lat_start is None and latitude >= lat_interval[0]:
            lat_start = i
        if lat_end is None and latitude >= lat_interval[1]:
            lat_end = i-1
            break
    if lat_start is None or lat_end is None:
        raise Exception('lat_interval has invalid interval')
    
    lon_start : int = None
    lon_end : int = None
    for i, longitude in enumerate(longitudes.split(', ')):
        longitude = float(longitude)
        if lon_start is None and longitude >= lon_interval[0]:
            lon_start = i
        if lon_end is None and longitude >= lon_interval[1]:
            lon_end = i-1
            break
    if lon_start is None or lon_end is None:
        raise Exception('lon_interval has invalid interval')
    
    return ((lat_start, lat_end), (lon_start, lon_end))


def parse_data(data : str, country : CountryCoastline):
    data = data.split('---------------------------------------------')[1]
    data = data.split('\n\n')
    _, *values = data[0].split('\n')
    level0 = [row.split(', ')[1:] for row in values if row[1].isdigit() and int(row[1]) == 0] #level 0 rows
    # for row in level0:
    #     print(row)
    
    #print(level0[2][4])
    
    levels = data[1].split('\n')[1].split(', ')
    longitudes = data[2].split('\n')[1].split(', ')
    longitudes = [int(float(val)) for val in longitudes]
    latitudes = data[3].split('\n')[1].split(', ')
    latitudes = [int(float(val)) for val in latitudes]

    pixel_values : Dict[Tuple, int] = {}
    data = []

    for i, row in enumerate(level0):
        for j, item in enumerate(row):
            data.append([latitudes[j], longitudes[i], float(item)])
    draw_on_map(data)
    
    #pixel_interval_h = country.horizontal_img_interval((int(float(longitudes[0])), int(float(longitudes[len(longitudes) - 1]))))
    #pixel_interval_v = country.vertical_img_interval((int(float(latitudes[0])), int(float(latitudes[len(latitudes) - 1]))))

#     for i, row in enumerate(level0):
#         for j, item in enumerate(row):
#             try:
#                 pixel_interval_h = country.horizontal_img_interval((longitudes[i], longitudes[i+1]))
#                 pixel_interval_v = country.vertical_img_interval((latitudes[j], latitudes[j+1]))
#             except IndexError:
#                 break
#             for y in range(*pixel_interval_v):
#                 for x in range(*pixel_interval_h):
#                     pixel_values[(x,y)] = int(float(item))

    # for y in range(pixel_interval_v[0], pixel_interval_v[1]):
    #     for x in range(pixel_interval_h[0], pixel_interval_h[1]):
    #         lon = country.longitude_from_img((x,x))[0]
    #         lat = country.latitude_from_img((y,y))[0]
    #         pixel_values[(x,y)] = float(level0[lon][lat])

    # for i, row in enumerate(level0):
    #     for j, item in enumerate(row):
    #         longitude = int(float(longitudes[i]))
    #         latitude = int(float(latitudes[j]))
    #         x = country.horizontal_img_pixel(longitude)
    #         y = country.vertical_img_pixel(latitude)
    #         value = int(float(item))
    #         pixel_values[(x, y)] = value

#     vals = [value for value in pixel_values.values()]
#     min_value = min(vals)
#     max_value = max(vals)
#     scatter = max_value - min_value
#     def value_to_color(value : int) -> List[int]:
#         percent = (value - min_value) / scatter
#         return int(255 * percent)

#     transperency = 0.7
#     country_img = country.coastline_image.copy()
#     print(len(pixel_values.keys()))
#     for (x, y) in pixel_values.keys():
#         color_value = value_to_color(pixel_values[(x, y)]) * transperency
#         country_img[y][x] += np.array([int(color_value), 0, 0], dtype=np.uint8)
#         #country_img[y][x] = [255,255,255]
#     resized = cv.resize(country_img, (int(0.45 * country_img.shape[1]), int(0.45 * country_img.shape[0])))
#     cv.imshow('country', resized)
#     cv.waitKey(0)
    #values = [[pixel, value] for value in data]
        

def request_magnetic_field(lon_interval : Tuple[int], lat_interval : Tuple[int],
                           country : CountryCoastline, level : Tuple[int] = (0, 0)):
    request_url = 'http://144.206.233.183/thredds/dodsC/Data/igrf11.nc.ascii?lat[0:1:180],lon[0:1:360],level[0:1:10]'
    response = req.get(request_url)
    lat_indices, lon_indices = parse_and_get_indices(response.text, lat_interval, lon_interval)
    data_request_url = ('http://144.206.233.183/thredds/dodsC/Data/igrf11.nc.ascii?' +
                            f'data[{level[0]}:1:{level[1]}]' +
                            f'[{lon_indices[0]}:1:{lon_indices[1]}]' +
                            f'[{lat_indices[0]}:1:{lat_indices[1]}]' )
    print(f'Performing request from url {data_request_url}')
    data_response = req.get(data_request_url)
    parse_data(data_response.text, country)


if __name__ == '__main__':
    # по пикселям получаем координаты
    # делаем запрос для получения lat lon, определяем индексы широт
    # эти индексы вставляем в data
    # по дате строим карту.
    # response = req.get('http://144.206.233.183/thredds/dodsC/Data/igrf11.nc.ascii?lat[0:1:180],lon[0:1:360],level[0:1:10],data[0:1:0][0:1:20][0:1:10]')
    coastline_img = cv.imread(COASTLINE_IMAGE_PATH)
    country = CountryCoastline(COUNTRY_BORDERS_LATITUDE, COUNTRY_BORDERS_LONGITUDE, coastline_img)
    request_magnetic_field((19, 169), (41, 82), country)


