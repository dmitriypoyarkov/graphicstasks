import folium
import pandas as pd
import folium.plugins

import branca
import branca.colormap as cm

from IPython.display import display

from typing import List


def draw_on_map(data : List[List[float]]):
    df = pd.DataFrame(data, columns=['latitude','longitude','power'])
    x_start = (df['latitude'].max() + df['latitude'].min()) / 2
    y_start = (df['longitude'].max() + df['longitude'].min()) / 2
    start_coord = (x_start, y_start)

    mapp = folium.Map(location=start_coord, zoom_start=1)
    
    lat = list(df.latitude)
    lon = list(df.longitude)
    my_pow = list(df.power)
    min_power = min(my_pow)
    max_power = max(my_pow)

    colormap = cm.LinearColormap(colors=['red','lightblue'], index=[min_power, max_power], vmin=min_power, vmax=max_power)
    for loc, p in zip(zip(lat, lon), my_pow):
        folium.Circle(
            location=loc,
            radius=30000,
            fill=True,
            color=colormap(p),
            rotation=45,
            fill_opacity=0.5,
            stroke=False
        ).add_to(mapp)
    print('hello!')
    mapp.add_child(colormap)
    display(mapp)


if __name__ == '__main__':
    data = [
        [33.823400, -118.12194, 99.23],
        [33.823500, -118.12294, 95.23],
        [33.823600, -118.12394, 91.23],
        [33.823700, -118.12494, 90.00]
    ]
    draw_on_map(data)

    