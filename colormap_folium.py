import folium
import pandas as pd
import folium.plugins

import branca
import branca.colormap as cm

from IPython.display import display

from geojson import Polygon, Feature, FeatureCollection
from typing import List



def draw_on_map(data : List[List[float]]):
    df = pd.DataFrame(data, columns=['latitude','longitude','power'])
    x_start = (df['latitude'].max() + df['latitude'].min()) / 2
    y_start = (df['longitude'].max() + df['longitude'].min()) / 2
    start_coord = (x_start, y_start)

    mapp = folium.Map(location=start_coord, zoom_start=1)
    
    lat = list(df.latitude)
    lon = list(df.longitude)
    powers = list(df.power)
    min_power = min(powers)
    max_power = max(powers)

    colormap = cm.LinearColormap(colors=['red','lightblue'], index=[min_power, max_power], vmin=min_power, vmax=max_power)
    for loc, p in zip(zip(lat, lon), powers):
        folium.Choropleth(
            location=loc,
            radius=60000,
            fill=True,
            color=colormap(p),
            rotation=45,
            fill_opacity=0.5,
            stroke=False
        ).add_to(mapp)
    print('hello!')
    mapp.add_child(colormap)


# "geometry":{"type":"Polygon","coordinates":[[[-87.359296,35.00118],[-85.606675,34.984749],[-85.431413,34.124869],[-85.184951,32.859696],[-85.069935,32.580372],[-84.960397,32.421541],[-85.004212,32.322956],[-84.889196,32.262709],[-85.058981,32.13674],[-85.053504,32.01077],[-85.141136,31.840985],[-85.042551,31.539753],[-85.113751,31.27686],[-85.004212,31.003013],[-85.497137,30.997536],[-87.600282,30.997536],[-87.633143,30.86609],[-87.408589,30.674397],[-87.446927,30.510088],[-87.37025,30.427934],[-87.518128,30.280057],[-87.655051,30.247195],[-87.90699,30.411504],[-87.934375,30.657966],[-88.011052,30.685351],[-88.10416,30.499135],[-88.137022,30.318396],[-88.394438,30.367688],[-88.471115,31.895754],[-88.241084,33.796253],[-88.098683,34.891641],[-88.202745,34.995703],[-87.359296,35.00118]]]}
    
    description = f'"type":"Feature","id":{id},"properties":{{"name":{id}}}'
    #geometry = f'"geometry" : {{"type" : "Polygon", "coordinates" : [[{coords_list}]]}}'
    features = []
    indices = []
    columns = ['id', 'value']
 
    for location in zip(lat, lon):
        polygon = Polygon([[location]])
        feature = Feature(geometry = polygon)


    
    data = pd.DataFrame(powers, index=indices, columns=columns)
    
    
    collection = FeatureCollection(features)
    folium.Choropleth(
        geo_data = collection,
        data = collection,
        columns=['state',"wills"],
        key_on="feature.id",
        fill_color='YlGnBu',
        fill_opacity=1,
        line_opacity=0.2,
        legend_name="wills",
        smooth_factor=0,
        Highlight= True,
        line_color = "#0000",
        name = "Wills",
        show=False,
        overlay=True,
        nan_fill_color = "White"
    )

    display(mapp)


if __name__ == '__main__':
    data = [
        [33.823400, -118.12194, 99.23],
        [33.823500, -118.12294, 95.23],
        [33.823600, -118.12394, 91.23],
        [33.823700, -118.12494, 90.00]
    ]
    draw_on_map(data)

    folium.Choropleth(

    )
    