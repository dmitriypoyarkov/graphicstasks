# Есть изображение. Взять и заменить 1 цвет на другой.
import cv2 as cv
import numpy as np
from enum import Enum


class ColorsToSwap(Enum):
    RED_BLUE = 0
    RED_GREEN = 1
    BLUE_GREEN = 2


def swap_colors(img_path : str, colors : ColorsToSwap = ColorsToSwap.RED_BLUE) -> np.ndarray:
    img : np.ndarray = cv.imread(img_path)
    height, width, _ = img.shape

    if colors == ColorsToSwap.RED_BLUE:
        new_img = np.array([
            [[r, g, b] for b, g, r in row] for row in img
        ])
    if colors == ColorsToSwap.RED_GREEN:
        new_img = np.array([
            [[b, r, g] for b, g, r in row] for row in img
        ])
    if colors == ColorsToSwap.BLUE_GREEN:
        new_img = np.array([
            [[g, b, r] for b, g, r in row] for row in img
        ])
    
    return new_img


if __name__ == "__main__":
    img_path = '/home/fc/Downloads/1.png'
    
    cv.imshow('Swapped Colors', swap_colors(img_path))
    cv.waitKey()
    # width, height = img.shape
    # for x in range()