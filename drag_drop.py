import sys, os
import graphics_task1 as swapper
import cv2 as cv
from PyQt5.QtWidgets import QApplication, QComboBox, QLayout, QSizePolicy, QWidget, QLabel, QVBoxLayout, QHBoxLayout
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QWindow


class ViewImageLabel(QLabel):
    def __init__(self):
        super().__init__()

        self.setAlignment(Qt.AlignCenter)
        self.setText('\n\n Swapped Image \n\n')
        self.setStyleSheet('''
            QLabel{
                border: 4px dashed #aaa
            }
        ''')

    def setPixmap(self, image):
        super().setPixmap(image)
        super().setMaximumSize(500, 500)
        super().setScaledContents(True)
        super().setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)


class DropImageLabel(QLabel):
    def __init__(self):
        super().__init__()

        self.setAlignment(Qt.AlignCenter)
        self.setText('\n\n Input Image \n\n')
        self.setStyleSheet('''
            QLabel{
                border: 4px dashed #aaa
            }
        ''')

    def setPixmap(self, image):
        super().setPixmap(image)
        super().setMaximumSize(500, 500)
        super().setScaledContents(True)
        super().setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)


class DragDropField(QWidget):
    def __init__(self):
        super().__init__()
        self.resize(400, 400)
        self.setAcceptDrops(True)

        mainLayout = QHBoxLayout()
        self.colors_to_swap : swapper.ColorsToSwap = swapper.ColorsToSwap.RED_BLUE
        self.img_path : str = ''

        self.dropImageField = DropImageLabel()
        self.showImageField = ViewImageLabel()
        mainLayout.addWidget(self.dropImageField)
        mainLayout.addWidget(self.showImageField)

        self.setLayout(mainLayout)

    def dragEnterEvent(self, event):
        if event.mimeData().hasImage:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasImage:
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        if event.mimeData().hasImage:
            event.setDropAction(Qt.CopyAction)
            file_path = event.mimeData().urls()[0].toLocalFile()
            self.img_path = file_path
            self.set_drop_field()
            self.set_show_field()
            event.accept()
        else:
            event.ignore()

    def set_drop_field(self):
        self.dropImageField.setPixmap(QPixmap(self.img_path))

    def set_show_field(self):
        if self.img_path == '':
            return
        new_img = swapper.swap_colors(self.img_path, self.colors_to_swap)
        file_name, *_, file_ext = os.path.splitext(self.img_path)
        swapped_file_path = file_name + '_swapped.' + file_ext
        cv.imwrite(swapped_file_path, new_img)
        self.showImageField.setPixmap(QPixmap(swapped_file_path))

    def set_colors_to_swap(self, colors : swapper.ColorsToSwap):
        self.colors_to_swap = colors


class DropDown(QWidget):
    def __init__(self, controllable_widget : DragDropField, parent = None):
        super().__init__(parent)
        
        layout = QHBoxLayout()
        self.cb = QComboBox()
        self.cb.addItem('Red-Blue')
        self.cb.addItem('Red-Green')
        self.cb.addItem('Blue-Green')
        self.cb.currentIndexChanged.connect(self.selectionchange)
        
        layout.addWidget(self.cb)
        self.setLayout(layout)
        self.setWindowTitle("combo box demo")

        self.controllable_widget = controllable_widget

    def selectionchange(self,i):
        print("Current index", i, "selection changed to", self.cb.currentText())
        new_colors = swapper.ColorsToSwap(i)
        self.controllable_widget.set_colors_to_swap(new_colors)
        self.controllable_widget.set_show_field()
      



if __name__ == '__main__':
    app = QApplication(sys.argv)

    drag_drop = DragDropField()
    dropdown_list = DropDown(drag_drop)
    dropdown_list.setMaximumHeight(40)

    layout = QVBoxLayout()
    layout.addWidget(dropdown_list)
    layout.addWidget(drag_drop)
    layout.setContentsMargins(0,0,0,0)

    window = QWidget();
    window.setWindowTitle('Color Swapper')
    window.setLayout(layout)

    window.show()
    sys.exit(app.exec_())