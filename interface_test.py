import sys
import graphics_task1 as swapper
import cv2 as cv
from PyQt5.QtWidgets import (QPushButton, QWidget,
    QLineEdit, QApplication, QGraphicsPixmapItem,
    QLabel, QGridLayout)
from PyQt5.QtGui import QPixmap
from drag_drop import DragDropField
# from PyQt5.QtWidgets import QApplication
# from PyQt5.QtWidgets import Qlabel
# from PyQt5.QtWidgets import QWidget

app_name = 'test'
img_path = '/home/fc/Downloads/1.png'
img_output = '/home/fc/Downloads/2.png'


if __name__ == "__main__":
    cv.imwrite(img_output, swapper.swap_colors(img_path))

    app = QApplication([app_name])
    window = QWidget()
    window.setWindowTitle('Test')
    window.setGeometry(0, 0, 700, 700)
    helloMsg = QLabel('Hello World!', parent=window)
    helloMsg.move(60, 15)

    layout = QGridLayout()
    label = QLabel('Text!')


    label.setPixmap(QPixmap(img_output))

    image_field = DragDropField()
    layout.addWidget(image_field, 1, 1)

    layout.addWidget(QLabel('Hello!'), 0, 1)
    layout.addWidget(QPushButton('Hello!'), 1, 0)
    #layout.addWidget(QPushButton('Hello!'), 1, 1)
    layout.addWidget(QPushButton('Hello!'), 1, 2)
    layout.addWidget(QPushButton('Hello!'), 2, 0)
    layout.addWidget(QPushButton('Hello!'), 2, 2)

    window.setLayout(layout)
    
    window.show()
    sys.exit(app.exec_())